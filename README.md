# master-bpmn-kontaktanfrage #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Dieses Modul beinhaltet das BPMN 2.0 Prozessmodell und Quellcode für die Service-Tasks. Zum Ausführen ist die Installation in einer Process Engine notwendig.
![BPMN Prozessmodell](http://www.cherriz.de/master/images/content/bpmn_process.png "BPMN Prozessmodell")

### Weiterführende Übersicht ###

Dieses Modul ist der im Architekturschaubild dargestellten hellorange BPMN 2.0 Prozess. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


