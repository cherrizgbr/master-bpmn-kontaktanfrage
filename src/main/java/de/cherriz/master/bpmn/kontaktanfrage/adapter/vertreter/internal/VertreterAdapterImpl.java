package de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.Vertreter;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.VertreterAdapter;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.VertreterAdapter}.
 *
 * @author Frederik Kirsch
 */
public class VertreterAdapterImpl extends AbstractService implements VertreterAdapter {

    private String pathGebiet = null;

    /**
     * Setzt den Pfad für die Funktion Vertreter zu Gebiet ermitteln.
     *
     * @param pathGebiet Der Pfad.
     */
    public void setPathGebiet(String pathGebiet) {
        this.pathGebiet = pathGebiet;
    }

    @Override
    public Vertreter getVertreterByGebiet(String strasse, String ort, Integer plz) {
        try {
            //Request absetzen
            String result = this.getTarget(this.pathGebiet.replace("{strasse}", strasse)
                    .replace("{ort}", ort)
                    .replace("{plz}", plz.toString()))
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(String.class);

            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(result, Vertreter.class);
        } catch (NotAcceptableException e) {
            //Wird bei illegaler addresse zurückgegeben
        } catch (IOException e) {
            //Echter Fehler
            e.printStackTrace();
        }
        return null;
    }

}