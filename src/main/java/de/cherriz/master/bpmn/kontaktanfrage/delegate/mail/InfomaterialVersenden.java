package de.cherriz.master.bpmn.kontaktanfrage.delegate.mail;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.MailAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Infomaterial versenden.
 *
 * @author Frederik Kirsch
 */
public class InfomaterialVersenden implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(InfomaterialVersenden.class.getName());

    /**
     * Loesstt die Versendung von Infomaterial an den Kontakt aus.
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Infomaterial versenden");

        //Adapter instanziieren
        MailAdapter mailAdapter = SpringContextRetriever.getSpringContext().getBean(MailAdapter.class);

        //Verarbeitung
        Long kontaktID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());
        mailAdapter.setInfomaterial(kontaktID);
    }

}
