package de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.internal;

/**
 * Bestaetigung für einen bestimmten Termin zwischen einem Kontakt und einem Vertreter.
 *
 * @author Frederik Kirsch
 */
public class Terminbestaetigung {

    private Long kontaktID = null;

    private Long vertreterID = null;

    private Long terminID = null;

    /**
     * Konstruktor.
     *
     * @param kontaktID Kontakt des Termins.
     * @param vertreterID Vertreter des Termins.
     * @param terminID Der Termin.
     */
    public Terminbestaetigung(Long kontaktID, Long vertreterID, Long terminID) {
        this.kontaktID = kontaktID;
        this.vertreterID = vertreterID;
        this.terminID = terminID;
    }

    /**
     * Der Kontakt des Termins.
     *
     * @return Der Kontakt.
     */
    public Long getKontaktID() {
        return kontaktID;
    }

    /**
     * Liefert den Vertreter des Termins.
     *
     * @return Der Vertreter.
     */
    public Long getVertreterID() {
        return vertreterID;
    }

    /**
     * Liefert die ID des Termins.
     *
     * @return Die TerminID.
     */
    public Long getTerminID() {
        return terminID;
    }
}