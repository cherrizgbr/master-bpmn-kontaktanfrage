package de.cherriz.master.bpmn.kontaktanfrage.adapter.user;

/**
 * Ein User.
 *
 * @author Frederik Kirsch
 */
public class User {

    private Long id = null;

    private String vorname = null;

    private String nachname = null;

    private String email = null;

    private String telefon = null;

    private String strasse = null;

    private String ort = null;

    private Integer plz = null;

    private String type = null;

    private Long vertreter = null;

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID des Users.
     *
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Den Vornamen.
     */
    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * @return Der Nachname
     */
    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    /**
     * @return Die Email.
     */
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return Die Telefonnummer.
     */
    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * @return Die Strasse.
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Setzt die Strasse.
     *
     * @param strasse Die Strasse.
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * @return Der Ort.
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Setzt den Ort.
     *
     * @param ort Der Ort.
     */
    public void setOrt(String ort) {
        this.ort = ort;
    }

    /**
     * @return Die PLZ.
     */
    public Integer getPlz() {
        return plz;
    }

    /**
     * Setzt die PLZ.
     *
     * @param plz Die PLZ.
     */
    public void setPlz(Integer plz) {
        this.plz = plz;
    }

    /**
     * @return Der Status.
     */
    public String getType() {
        return type;
    }

    /**
     * Setzt den Status.
     *
     * @param type Der Status.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Der Vertreter.
     */
    public Long getVertreter() {
        return vertreter;
    }

    /**
     * Setzt den Vertreter.
     * @param vertreter Der Vertreter.
     */
    public void setVertreter(Long vertreter) {
        this.vertreter = vertreter;
    }

}