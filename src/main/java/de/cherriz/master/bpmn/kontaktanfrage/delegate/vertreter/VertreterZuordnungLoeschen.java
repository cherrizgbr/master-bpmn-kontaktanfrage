package de.cherriz.master.bpmn.kontaktanfrage.delegate.vertreter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Vertreterzuordnung löschen.
 *
 * @author Frederik Kirsch
 */
public class VertreterZuordnungLoeschen implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(VertreterZuordnungLoeschen.class.getName());

    /**
     * Keine Verarbeitung.
     * Dieser Schritt erfolgt automatisiert in der aufrufenden Anwendung.
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Vertreterzuordnung loeschen");

        //Nichts zu tun, passiert automatisch
    }

}
