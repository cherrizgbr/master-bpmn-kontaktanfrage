package de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag;

/**
 * Schnittstelle für den Auftragsservice.
 *
 * @author Frederik Kirsch
 */
public interface AuftragAdapter {

    /**
     * Liefert die Auftragsdaten zu einer AuftragsID.
     * @param auftragID Die AuftragsID.
     * @return Die Auftragsdaten
     */
    public Auftrag getAuftragsDaten(Long auftragID);

}