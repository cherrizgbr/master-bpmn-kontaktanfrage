package de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter;

/**
 * Ein Vertreter.
 *
 * @author Frederik Kirsch
 */
public class Vertreter {

    private Long id = null;

    private Long gebiet = null;

    private String vorname = null;

    private String nachname = null;

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID.
     *
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Das Gebiet.
     */
    public Long getGebiet() {
        return gebiet;
    }

    /**
     * Setzt das Gebiet.
     *
     * @param gebiet Das Gebiet.
     */
    public void setGebiet(Long gebiet) {
        this.gebiet = gebiet;
    }

    /**
     * @return Der Vorname.
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Setzt den Vornamen.
     *
     * @param vorname Der Vorname.
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * @return Der Nachname.
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * Setzt den Nachnamen.
     *
     * @param nachname Der Nachname.
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

}