package de.cherriz.master.bpmn.kontaktanfrage.delegate.vertreter;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Vertreter zuordnen.
 *
 * @author Frederik Kirsch
 */
public class VertreterZuordnen implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(VertreterZuordnen.class.getName());

    /**
     * Speichert den Vertreter aus dem Context über einen Serviceaufruf am User aus dem Context.
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Vertreter zuordnen");

        //Prozessdaten auslesen
        Long kontaktID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());
        Long vertreterID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.VERTRETERID.toString()).toString());

        //User aktualisieren
        UserAdapter userAdapter = SpringContextRetriever.getSpringContext().getBean(UserAdapter.class);
        userAdapter.setVertreter(kontaktID, vertreterID);
    }

}
