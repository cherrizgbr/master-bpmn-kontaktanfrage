package de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter;

/**
 * Schnittstelle für den VertreterService.
 *
 * @author Frederik Kirsch
 */
public interface VertreterAdapter {

    /**
     * Liefert zu einer Adresse den zugeordneten Vertreter.
     *
     * @param strasse Die Strasse.
     * @param ort Der Ort.
     * @param plz Die PLZ.
     * @return Der zugeordnete Vertreter.
     */
    Vertreter getVertreterByGebiet(String strasse, String ort, Integer plz);

}