package de.cherriz.master.bpmn.kontaktanfrage.delegate.kontakt;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Kontakt loeschen.
 *
 * @author Frederik Kirsch
 */
public class KontaktLoeschen implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(KontaktLoeschen.class.getName());

    /**
     * Löscht einen Kontakt aus der Datenbank.
     * Dieser Vorgang ist nur notwendig wenn der Kontakt vorab gespeichert wurde (Status="Unique").
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Kontakt loeschen");

        // Gelöscht werden muss nur, wenn der user nicht als Duplette erkannt wurde.
        // Dann löscht der Service automatisch.
        String kontaktStatus = execution.getVariable(KontaktanfrageAttributes.KONTAKTSTATUS.toString()).toString();
        if ("Unique".equals(kontaktStatus)) {
            //Kontakt löschen
            UserAdapter userAdapter = SpringContextRetriever.getSpringContext().getBean(UserAdapter.class);
            Long kundenID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());
            userAdapter.delete(kundenID);
        }

    }

}