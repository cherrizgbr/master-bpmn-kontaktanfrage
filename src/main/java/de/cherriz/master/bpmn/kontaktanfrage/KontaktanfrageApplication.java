package de.cherriz.master.bpmn.kontaktanfrage;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

/**
 * Einstiegsklasse für den Prozess.
 *
 * @author Frederik Kirsch
 */
@ProcessApplication("Kontaktanfrage")
public class KontaktanfrageApplication extends ServletProcessApplication {
}
