package de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.MailAdapter;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.MailAdapter}.
 *
 * @author Frederik Kirsch
 */
public class MailAdapterImpl extends AbstractService implements MailAdapter {

    private String pathTermin = null;

    private String pathInfo = null;

    /**
     * Setzt den Pfad für die Funktion Infomaterial versenden.
     *
     * @param pathInfo Der Pfad.
     */
    public void setPathInfo(String pathInfo) {
        this.pathInfo = pathInfo;
    }

    /**
     * Setzt den Pfad für die Funktion Terminbestaetigung versenden.
     *
     * @param pathTermin Der Pfad.
     */
    public void setPathTermin(String pathTermin) {
        this.pathTermin = pathTermin;
    }

    @Override
    public void sendTerminbestaetigung(Long kontaktID, Long vertreterID, Long terminID) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String para = mapper.writeValueAsString(new Terminbestaetigung(kontaktID, vertreterID, terminID));
            //Request absetzen
            this.getTarget(this.pathTermin)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .put(Entity.json(para), String.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setInfomaterial(Long kontaktID) {
        //Request absetzen
        this.getTarget(this.pathInfo.replace("{id}", kontaktID.toString()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.json(""), String.class);
    }

}