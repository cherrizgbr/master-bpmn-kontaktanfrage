package de.cherriz.master.bpmn.kontaktanfrage.delegate.termin;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Termin speichern.
 *
 * @author Frederik Kirsch
 */
public class TerminSpeichern implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(TerminSpeichern.class.getName());

    /**
     * Verarbeitet den Rückgabewert der vorangegangenen Usertask und speichert dem Termin in den Prozessvariablen.
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        //TerminID aus Rückgabewert extrahieren
        String terminStr = execution.getVariable(KontaktanfrageAttributes.TASKRESULT.toString()).toString();
        int idx = terminStr.indexOf(":");
        String terminID = terminStr.substring(idx + 1);

        //TerminID in Prozessdaten setzen
        execution.setVariable(KontaktanfrageAttributes.TERMINID.toString(), terminID);

        LOGGER.info("Termin speichern [Input: " + terminStr + " Output: " + terminID+"]");
    }

}
