package de.cherriz.master.bpmn.kontaktanfrage.delegate.kontakt;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Kontakt ermitteln.
 *
 * @author Frederik Kirsch
 */
public class KontaktErmitteln implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(KontaktErmitteln.class.getName());

    /**
     * Ermittelt den originalusre aus dem Rückgabewert des vorangegangenen Usertasks.
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Kontakt ermitteln");

        //Kontakt aus Verarbeitungsergebnis setzen.
        String origUserID = execution.getVariable(KontaktanfrageAttributes.TASKRESULT.toString()).toString();
        execution.setVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString(), origUserID);
    }

}
