package de.cherriz.master.bpmn.kontaktanfrage.adapter.user;

/**
 * Schnittstelle für den UserService.
 *
 * @author Frederik Kirsch
 */
public interface UserAdapter {

    /**
     * Liefert einen User zu einer ID.
     * @param id Die ID.
     * @return Der User.
     */
    User get(Long id);

    /**
     * Löscht den User zu einer ID.
     * @param id Die ID.
     * @return Das gelöschte Userobjekt.
     */
    User delete(Long id);

    /**
     * Speichert den uebergeben User in der Datenbank.
     *
     * @param user Der User.
     * @return Der User ergänzt um ID des Datenbankeintrags.
     */
    User userAnlagen(User user);

    /**
     * Setzt den Status eines Users.
     *
     * @param userID Die ID des Users
     * @param status Der neue Status.
     */
    void setStatus(Long userID, String status);

    /**
     * Ordnet einem User einen Vertreter zu.
     * @param userID Der User.
     * @param vertreterID Der Vertreter.
     */
    void setVertreter(Long userID, Long vertreterID);

}