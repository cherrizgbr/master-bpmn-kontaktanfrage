package de.cherriz.master.bpmn.kontaktanfrage;

/**
 * Beschreibung der verschiedenen Prozessattribute.
 *
 * @author Frederik Kirsch
 */
public enum KontaktanfrageAttributes {

    KONTAKTSTATUS("kontaktstatus"),

    TERMINSTATUS("terminstatus"),

    AUFTRAGSDATEN("auftragsdaten"),

    KUNDENKONTAKTID("kundenkontaktid"),

    VERTRETERID("vertreterid"),

    TERMINID("terminid"),

    TASKRESULT("taskResult");

    private final String name;

    /**
     * Konstruktor.
     * @param name
     */
    KontaktanfrageAttributes(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
