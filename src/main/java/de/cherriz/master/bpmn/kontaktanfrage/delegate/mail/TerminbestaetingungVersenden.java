package de.cherriz.master.bpmn.kontaktanfrage.delegate.mail;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.mail.MailAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Terminbestaetigung versenden.
 *
 * @author Frederik Kirsch
 */
public class TerminbestaetingungVersenden implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(TerminbestaetingungVersenden.class.getName());

    /**
     * Loesst die Versendung einer Terminbestätigung an den Kontakt aus.
     *
     * @param execution Der Prozesscontext
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Terminbestaetigung Versenden");

        //Adapter instanziieren
        MailAdapter mailAdapter = SpringContextRetriever.getSpringContext().getBean(MailAdapter.class);

        //Daten aus Prozess auslesen
        Long kontaktID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());
        Long vertreterId = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.VERTRETERID.toString()).toString());
        Long terminID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.TERMINID.toString()).toString());

        //Terminbestaetigung versenden
        mailAdapter.sendTerminbestaetigung(kontaktID, vertreterId, terminID);
    }

}
