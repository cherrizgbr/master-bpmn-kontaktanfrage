package de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repräsentiert den Auftrag für die Prozessinstanziierung.
 * Enthält die Daten für die Verarbeitung
 *
 * @author Frederik Kirsch
 */
public class Auftrag {

    private Long id = null;

    private String prozess = null;

    private Map<String, String> daten = null;

    /**
     * Setzt die AuftragsID.
     * @param id Die AuftragsID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Setzt den Prozess, durch den der Auftrag verarbeitet werden soll..
     * @param prozess Der Prozess.
     */
    public void setProzess(String prozess) {
        this.prozess = prozess;
    }

    /**
     * Setzt die Auftragsdaten die verarbeitet werden sollen.
     * @param auftragsDaten Die Auftragsdaten.
     */
    public void setAuftragsDaten(List<Auftragsdatum> auftragsDaten){
        this.daten = new HashMap<String, String>();
        for(Auftragsdatum item : auftragsDaten){
            this.daten.put(item.attribute, item.value);
        }
    }

    /**
     * @return Die AuftragsID.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @return Der Name des verarbeitenden Prozesses.
     */
    public String getProzess() {
        return this.prozess;
    }

    /**
     * Liefert den Wert zu einem Auftragsattribut.
     * @param attribute Der Attributname.
     * @return Der Wert des Attributs.
     */
    public String getValue(String attribute) {
        return this.daten.get(attribute);
    }

}