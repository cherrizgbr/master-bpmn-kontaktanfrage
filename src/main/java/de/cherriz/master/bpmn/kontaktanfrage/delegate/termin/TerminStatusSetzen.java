package de.cherriz.master.bpmn.kontaktanfrage.delegate.termin;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Terminstatus setzen.
 *
 * @author Frederik Kirsch
 */
public class TerminStatusSetzen implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(TerminSpeichern.class.getName());

    /**
     * Verarbeitet den Rückgabewert der vorangegangen Usertask.  Ermittelt ob ein Termin vereinbart wurde oder nicht.
     * Möglichkeiten sind:
     * <ul>
     *     <li>Termin erwünscht</li>
     *     <li>Kein Termin, unbefugte Registrierung</li>
     *     <li>Kein Termin, nur Infomaterial zusenden</li>
     *     <li>Kontakt existiert nicht, Fehler</li>
     * </ul>
     *
     * @param execution Der Prozesscontext.
     * @throws Exception Der Kontakt existiert nicht.
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Terminstatus setzen");

        String ergPruefung = execution.getVariable(KontaktanfrageAttributes.TASKRESULT.toString()).toString();

        String status = null;
        if(ergPruefung.equals("Info")){
            //Infomaterial versenden
            status = "Unerwuenscht";
        }else if(ergPruefung.startsWith("Termin")){
            //Termin speichern
            status = "Gewuenscht";
        }else if(ergPruefung.equals("Kein Termin")){
            //Fake Kontakt
            status = "Unbefugt";
        }else{
            throw new BpmnError("FAKE_CONTACT");
        }

        execution.setVariable(KontaktanfrageAttributes.TERMINSTATUS.toString(), status);
    }
}
