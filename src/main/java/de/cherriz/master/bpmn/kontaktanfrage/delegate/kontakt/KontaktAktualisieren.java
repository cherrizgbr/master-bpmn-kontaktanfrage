package de.cherriz.master.bpmn.kontaktanfrage.delegate.kontakt;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Kontakt aktualisieren.
 *
 * @author Frederik Kirsch
 */
public class KontaktAktualisieren implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(KontaktAktualisieren.class.getName());

    /**
     *Verarbeitet das Ergebnis der vorangegangenen Usertask.
     * Speicher den entgültigen Status des Kontakts.
     * Möglich sind:
     * <ul>
     *     <li>Neuer Kontakt.</li>
     *     <li>Duplikat, bestehender Kontakt..</li>
     * </ul>
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Kontakt aktualisieren");

        //Services instanziieren
        UserAdapter userAdapter = SpringContextRetriever.getSpringContext().getBean(UserAdapter.class);

        //Prozessdaten auslesen
        String ergPruefung = execution.getVariable(KontaktanfrageAttributes.TASKRESULT.toString()).toString();
        Long kundenID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());

        //Verarbeitung
        String status = null;
        if(ergPruefung.equals("New")){
            //Neuer User
            status = "Unique";
        }else{
            //Duplette
            status = "Duplicate";

        }

        //Ergebnis speichern
        execution.setVariable(KontaktanfrageAttributes.KONTAKTSTATUS.toString(), status);
        userAdapter.setStatus(kundenID, status);
    }

}
