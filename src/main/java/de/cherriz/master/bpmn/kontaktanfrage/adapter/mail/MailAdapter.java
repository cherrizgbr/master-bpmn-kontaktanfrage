package de.cherriz.master.bpmn.kontaktanfrage.adapter.mail;

/**
 * Schnittstelle für den MailService.
 *
 * @author Frederik Kirsch
 */
public interface MailAdapter {

    /**
     * Versendet eine Terminbestaetigung an den entsprechenden Kontakt.
     * Inhalt sind der zugeordnete Vertreter und der Termin.
     *
     * @param kontaktID Der Empfaenger.
     * @param vertreterID Der zugeordnete Vertreter.
     * @param terminID Der Termin.
     */
    void sendTerminbestaetigung(Long kontaktID, Long vertreterID, Long terminID);

    /**
     * Versendet Infomaterial an den Kontakt.
     *
     * @param kontaktID Der Kontakt.
     */
    void setInfomaterial(Long kontaktID);

}