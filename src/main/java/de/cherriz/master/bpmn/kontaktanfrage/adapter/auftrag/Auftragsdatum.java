package de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Repräsentiert ein einzelnes Attribut eines Auftrags.
 *
 * @author Frederik Kirsch
 */
@JsonIgnoreProperties({"id", "auftragID"})
public class Auftragsdatum {

    /**
     * Der Attributname.
     */
    @JsonProperty
    public String attribute;

    /**
     * Der Attributwert.
     */
    @JsonProperty
    public String value;

}
