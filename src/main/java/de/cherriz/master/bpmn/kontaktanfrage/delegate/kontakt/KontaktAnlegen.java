package de.cherriz.master.bpmn.kontaktanfrage.delegate.kontakt;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.Auftrag;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.AuftragAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.User;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Kontakt anlegen.
 *
 * @author Frederik Kirsch
 */
public class KontaktAnlegen implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(KontaktAnlegen.class.getName());

    /**
     * Ermittelt einen Kontakt aus den Auftragsdaten und speichert ihn.
     * Das Ergebnis wird in den Prozessdaten hinterlegt.
     *
     * @param execution Der Prozesscontext.
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Kontakt anlegen");

        //Adapter instanziieren
        AuftragAdapter auftragAdapter = SpringContextRetriever.getSpringContext().getBean(AuftragAdapter.class);
        UserAdapter userAdapter = SpringContextRetriever.getSpringContext().getBean(UserAdapter.class);

        //AuftragsID aus Prozessdaten auslesen
        Long auftragsID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.AUFTRAGSDATEN.toString()).toString());

        //User aus Auftragsdaten aufbereiten
        Auftrag auftrag = auftragAdapter.getAuftragsDaten(auftragsID);
        User user = new User();
        user.setVorname(auftrag.getValue("vorname"));
        user.setNachname(auftrag.getValue("nachname"));
        user.setEmail(auftrag.getValue("email"));
        user.setTelefon(auftrag.getValue("telefon"));
        user.setStrasse(auftrag.getValue("strasse"));
        user.setPlz(Integer.valueOf(auftrag.getValue("plz")));
        user.setOrt(auftrag.getValue("ort"));

        //User Speichern und ergebnis auslesen
        User result = userAdapter.userAnlagen(user);
        Long id = result.getId();
        String status = result.getType();

        //Prozessdaten speichern
        execution.setVariable(KontaktanfrageAttributes.KONTAKTSTATUS.toString(), status);
        execution.setVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString(), id);
    }

}
