package de.cherriz.master.bpmn.kontaktanfrage.adapter.user.internal;

import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.User;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter}.
 *
 * @author Frederik Kirsch
 */
public class UserAdapterImpl extends AbstractService implements UserAdapter {

    private String pathCreate = null;

    private String pathStatus = null;

    private String pathVertreter = null;

    /**
     * Setzt den Pfad für die Funktion User anlegen.
     *
     * @param pathCreate Der Pfad.
     */
    public void setPathCreate(String pathCreate) {
        this.pathCreate = pathCreate;
    }

    /**
     * Setzt den Pfad für die Funktion Status setzen.
     *
     * @param pathStatus Der Pfad.
     */
    public void setPathStatus(String pathStatus) {
        this.pathStatus = pathStatus;
    }

    /**
     * Setzt den Pfad für die Funktion Vertreter zuordnen.
     *
     * @param pathVertreter Der Pfad.
     */
    public void setPathVertreter(String pathVertreter) {
        this.pathVertreter = pathVertreter;
    }

    @Override
    public User get(Long id) {
        //Request absetzen
        String result = this.getTarget(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(result, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User delete(Long id) {
        //Request absetzen
        String result = this.getTarget(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete(String.class);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(result, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User userAnlagen(User user) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String para = mapper.writeValueAsString(user);

            //Request absetzen
            String result = this.getTarget(this.pathCreate)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .put(Entity.json(para), String.class);

            return mapper.readValue(result, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setStatus(Long userID, String status) {
        ObjectMapper mapper = new ObjectMapper();

        //Request absetzen
        String result = this.getTarget(this.pathStatus.replace("{id}", userID.toString()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(status), String.class);

        try {
            mapper.readValue(result, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setVertreter(Long userID, Long vertreter) {
        ObjectMapper mapper = new ObjectMapper();

        //Request absetzen
        String result = this.getTarget(this.pathVertreter.replace("{id}", userID.toString()))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(vertreter.toString()), String.class);

        try {
            mapper.readValue(result, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}