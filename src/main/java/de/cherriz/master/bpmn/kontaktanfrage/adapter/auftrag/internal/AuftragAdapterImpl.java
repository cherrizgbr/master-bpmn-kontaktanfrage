package de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.internal;


import de.cherriz.master.basic.service.AbstractService;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.Auftrag;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.AuftragAdapter;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Implementierung des {@link de.cherriz.master.bpmn.kontaktanfrage.adapter.auftrag.AuftragAdapter}.
 *
 * @author Frederik Kirsch
 */
public class AuftragAdapterImpl extends AbstractService implements AuftragAdapter {

    @Override
    public Auftrag getAuftragsDaten(Long auftragID) {
        //Request absetzen
        String result = this.getTarget(auftragID.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(String.class);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(result, Auftrag.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}