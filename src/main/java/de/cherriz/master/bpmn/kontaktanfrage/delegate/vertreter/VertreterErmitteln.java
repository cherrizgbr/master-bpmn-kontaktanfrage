package de.cherriz.master.bpmn.kontaktanfrage.delegate.vertreter;

import de.cherriz.master.bpmn.kontaktanfrage.KontaktanfrageAttributes;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.User;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.user.UserAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.Vertreter;
import de.cherriz.master.bpmn.kontaktanfrage.adapter.vertreter.VertreterAdapter;
import de.cherriz.master.bpmn.kontaktanfrage.util.spring.SpringContextRetriever;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

/**
 * Implementierung für den Prozessschritt Vertreter ermitteln.
 *
 * @author Frederik Kirsch
 */
public class VertreterErmitteln implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger(VertreterErmitteln.class.getName());

    /**
     * Lädt auf Basis des Users den entsprechenden Vertreter.
     * Existiert die Adresse nicht wird eine Exception geworfen
     *
     * @param execution Der Prozesscontext.
     * @throws Exception Adresse des Users existiert nicht.
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info("Vertreter ermitteln");

        //Adapter instanziieren
        UserAdapter userAdapter = SpringContextRetriever.getSpringContext().getBean(UserAdapter.class);
        VertreterAdapter vertreterAdapter = SpringContextRetriever.getSpringContext().getBean(VertreterAdapter.class);

        //Abfrage vorbereiten
        Long kontaktID = Long.valueOf(execution.getVariable(KontaktanfrageAttributes.KUNDENKONTAKTID.toString()).toString());
        User kontakt = userAdapter.get(kontaktID);
        Vertreter vertreter = vertreterAdapter.getVertreterByGebiet(kontakt.getStrasse(), kontakt.getOrt(), kontakt.getPlz());

        //Ergebnisverarbeitung
        if (vertreter == null) {
            throw new BpmnError("FAKE_ADDRESS");
        }
        execution.setVariable(KontaktanfrageAttributes.VERTRETERID.toString(), vertreter.getId());
    }
}
