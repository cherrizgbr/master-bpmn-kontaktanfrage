package de.cherriz.master.bpmn.kontaktanfrage.util.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Kapselt den Zugriff auf den Spring-Context.
 * Sorgt dafür das der Context nur einmalig initialisiert wird.
 *
 * @author Frederik Kirsch
 */
public class SpringContextRetriever {

    private static ApplicationContext springContext = null;

    /**
     * Liefert den Spring-Context des Prozesses. Beim ersten Aufruf wird der Context geladen.
     *
     * @return Der Spring-Context.
     */
    public static final ApplicationContext getSpringContext() {
        if (springContext == null) {
            springContext = new ClassPathXmlApplicationContext("/context.xml");
        }
        return springContext;
    }

}
